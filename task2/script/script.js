function getJSONdata(url) {
  console.log("Write the result from "+ url +" into the #datatable");

}

getJSONdata("https://jsonplaceholder.typicode.com/users");

/*
* Calling JSON API
*/
$(function(){
  $.getJSON("https://jsonplaceholder.typicode.com/users", function(data){
    // Get the element with #datatable and set the inner text to the result.
    $("#datatable").text(data);
    console.log(data);

    var str = JSON.stringify(data);
    $("#datatable").append(str);

    // output into HTML
    var contact = JSON.parse(str);
    $("#datatable").append(contact);

      for (var i=0; i<contact.length; i++){
        // Username
        document.write("User["+i+"] - Username : "+contact[i].username+"<br>");

        /*
        * E-Mail & sending email by click
        */
        document.write("User["+i+"] - E-Mail : <a href='mailto:"+ contact[i].email+"'>"+contact[i].email+"</a><br>");

        /*
        * Street & open Google Maps by clicking
        */
        document.write("User["+i+"] - Street : <a target='_blank' href='https://www.google.de/maps/place/"+contact[i].address.geo.lat+","+contact[i].address.geo.lng+"'>"+contact[i].address.street+"</a><br>");

        // City
        document.write("User["+i+"] - City : "+contact[i].address.city+"<br>");

        //Company
        document.write("User["+i+"] - Company : "+contact[i].company.name+"<br>");

        //Company catch phrase
        document.write("User["+i+"] - Company catch phrase : "+contact[i].company.catchPhrase+"<br><br>");
      }
  });
});
